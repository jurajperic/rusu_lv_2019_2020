import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 

cars = pd.read_csv('resources\mtcars.csv')

filter = cars[(cars.cyl==4) | (cars.cyl==6) | (cars.cyl==8)]
plt.figure(0)
filter.mpg.plot.bar()
plt.show()

dict = {"4":np.array(cars[cars.cyl==4].wt), "6":np.array(cars[cars.cyl==6].wt), "8":np.array(cars[cars.cyl==8].wt)}
fig,ax = plt.subplots()
ax.boxplot(dict.values())
ax.set_xticklabels(dict.keys())
plt.show()

array = cars.groupby("am").mpg.mean()
plt.figure(2)
array.plot.bar()
plt.show()

manual = cars[cars.am == 0]
automatic = cars[cars.am == 1]
plt.figure(3)
plt.scatter(manual["qsec"], manual["hp"], label = "manual")
plt.scatter(automatic["qsec"], automatic["hp"], label = "automatic")
plt.legend()
plt.xlabel("qsec")
plt.ylabel("hp")
plt.show()