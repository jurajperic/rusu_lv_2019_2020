import pandas as pd

cars = pd.read_csv('resources\mtcars.csv')

print("1.")
mpg_sort = cars.sort_values('mpg', ascending=False)[:5]
print(mpg_sort)

print("2.")
low_mpg_8_cyl = cars[cars['cyl'] == 8].sort_values('mpg')[:5]
print(low_mpg_8_cyl)

print("3.")
avg_6_cyl = cars[cars['cyl']==6]['mpg'].mean()
print(avg_6_cyl)

print("4.")
avg_4_cyl_mass = cars[(cars.cyl==4) & (cars.wt*1000 >=2000) & (cars.wt*1000 <=2200)].mpg.mean() #multiply with 1000 because of '.' in csv
print(avg_4_cyl_mass)

print("5.")
print("Rucni mjenjac " + str(cars[cars.am == 0].car.count()))
print("Automatski mjenjac " + str(cars[cars.am == 1].car.count()))

print("6.")
print(cars[(cars.am == 1) & (cars.hp >=100)].car.count())

print("7.")
cars['kg'] = cars.wt * 1000 * 0.453592
print(cars.kg)