import urllib.request
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=0&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)
df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root[i]
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df.loc[len(df.index)]=row_s
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, errors='coerce', utc=True, format='%Y-%m-%d %H:%M:%S')
df.plot(y='mjerenje', x='vrijeme')

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

print(df.sort_values("mjerenje", ascending=False)[:3]["vrijeme"])

#df[df["mjerenje"] == None].groupby("month").count().plot().bar()

dict = {"ljeto":np.array(df[df['month']==6]["mjerenje"]), "zima":np.array(df[df['month']==12]["mjerenje"])}
fig,ax = plt.subplots()
ax.boxplot(dict.values())
ax.set_xticklabels(dict.keys())

working = df[(df.dayOfweek >=1) & (df.dayOfweek <=5)].mjerenje.mean()
weekend = df[(df.dayOfweek == 6) | (df.dayOfweek == 7)].mjerenje.mean()
array = [working,weekend]
labels =["working","weekend"]
plt.figure()
plt.bar(labels,array)
plt.show()