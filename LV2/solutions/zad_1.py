import re

file_name = input()
try:
  with open(file_name,'r') as f:
    text = f.read()
    addreses = re.findall('<*(\S*)@\S+', text)
    print(addreses)
    #unique
    #print(set(addreses))
except OSError:
  print("File not found")