import re

file_name = "LV2\\resources\\mbox-short.txt"
try:
  with open(file_name,'r') as f:
    text = f.read()
    while 1:
        mode = int(input())
        if mode >= 1 and mode <=5:
            break
    if mode == 1:
        addreses = re.findall('<*\S*a\S*@\S+', text)
        print(addreses)
    elif mode == 2:
        addreses = re.findall('<*\\b([^aA\s]*[aA][^aA\s]*\\b)@\S+', text)
        print(addreses)
    elif mode == 3:
        addreses = re.findall('<*(\\b[^\saA]+\\b)@\S+', text)
        print(addreses)
    elif mode == 4:
        addreses = re.findall('<*(\S*[0-9]+\S*)@\S+', text)
        print(addreses)
    elif mode == 5:
        addreses = re.findall('<*([a-z]+)@\S+', text)
        print(addreses)
except OSError:
  print("File not found")