import numpy as np
import matplotlib.pyplot as plt 
dice = np.random.randint(1,7,100)
np.histogram(dice, bins=8, range = [1,7])
plt.hist(dice)
plt.show()