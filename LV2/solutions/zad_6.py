import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
img = mpimg.imread('LV2\\resources\\tiger.png')
plt.figure()
plt.imshow(img)
# width, height, colors = img.shape
# for y in range(height):
#   for x in range(width):
#     for color in range(colors):
#       img[x][y][color] = img[x][y][color] / 1.5
#       if img[x][y][color]  > 1:
#           img[x][y][color]  = 1
img[::,::,::] /=2
img[img > 1] = 1
plt.figure()
plt.imshow(img)
plt.show()