import numpy as np
import matplotlib.pyplot as plt 

def avg(heights):
    return np.mean(heights)

people = np.random.randint(2,size=20)

heightsM = []
heightsF = []
for person in people:
    if person:
        heightsM.append(np.random.normal(180,7))
    else:
        heightsF.append(np.random.normal(167,7))

plt.plot(heightsM, 'b')
plt.plot(heightsF, 'r')
plt.axhline(y=avg(heightsM), color ='b',linestyle='--')
plt.axhline(y=avg(heightsF), color ='r',linestyle='--')
plt.show()

