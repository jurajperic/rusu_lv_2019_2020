import csv
import numpy as np
import matplotlib.pyplot as plt 

file_name = 'LV2\\resources\\mtcars.csv'
try:
  with open(file_name,'r') as f:
    csvreader = csv.DictReader(f)
    mpgs = []
    hps = []
    wts = []
    for item in csvreader:
      mpgs.append(float(item["mpg"])) 
      hps.append(float(item["hp"])) 
      wts.append(float(item["wt"]))
    plt.scatter(mpgs, hps, color = 'b', label = "mpgs-hps")
    plt.scatter(mpgs, wts, color = 'r', label = "mpgs-wts")
    plt.legend()
    plt.show()

    print("Min: " + str(min(mpgs)))
    print("Max: " + str(max(mpgs)))
    print("Mean: " + str(np.mean(mpgs)))

except OSError:
  print("File not found")